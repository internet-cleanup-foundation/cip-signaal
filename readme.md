CIP Signaal
===========

Ieder half jaar stuurt het CIP een mailtje met daarin de stand van de zaken van internet.nl en basisbeveiliging.

Dit scriptje gebruikt de basisbeveiliging API om dit rapport samen te stellen.


Wijzigingen
------

April 2024:
- Overgegaan naar XLSX formaat omdat het maximaal aantal rijen van XLS is bereikt in de 'everything' categorie
- De lege 'domains' kolom weggegooid


September 2023:
- Script gemaakt
- Dubbelingen etc zijn de bedoeling. Je krijgt de score van de organisatie te zien op basis van het domein. Het domein
is de match.
