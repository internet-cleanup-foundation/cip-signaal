import requests
from datetime import datetime
import os
from typing import List, Dict, Any
import pyexcel


WSM_INSTALLATION = "https://basisbeveiliging.nl/data"


def get_layers(country_code: str = "NL") -> List[str]:
    config = requests.get(f"{WSM_INSTALLATION}/config/")
    return config.json()["country_and_layers"][country_code]["layers"]


def get_organizations_on_layer(country_code: str = "NL", layer_name: str = "municipality", at_when: str = "0") -> List[Dict[str, Any]]:
    map_data = requests.get(f"{WSM_INSTALLATION}/map/{country_code}/{layer_name}/{at_when}/")
    return map_data.json()["features"]


def get_report_data(organization_id: int, country_code: str = "NL", layer_name: str = "municipality", at_when: str = "0") -> Dict[str, Any]:
    report_data = requests.get(f"{WSM_INSTALLATION}/report/{country_code}/{layer_name}/{organization_id}/{at_when}/")
    return report_data.json()


def create_cip_record(report_data: Dict[str, Any], organization_id, organization_name):
    # Metrics contain the following stuff, which might be useful:
    # scan, comply or explain, evidence, impact and more.
    cip_records = []

    for url in report_data["calculation"]["organization"]["urls"]:
        cip_record = {
            "link": f"https://basisbeveiliging.nl/#/report/{organization_id}/",
            "high": report_data["calculation"]["organization"]["high"],
            "medium": report_data["calculation"]["organization"]["medium"],
            "low": report_data["calculation"]["organization"]["low"],
            "ok": report_data["calculation"]["organization"]["ok"],
            "domain": url["url"],
            "organizatie": organization_name
        }

        cip_records.append(cip_record)

    return cip_records


def get_metrics(country_code: str = "NL", layer_names: List[str] = [], at_when: str = "0") -> None:

    # dump everything in a directory of the current date in iso format
    date = str(datetime.now().date())
    os.makedirs(date, exist_ok=True)

    everything = []

    for layer_name in layer_names:
        organizations = get_organizations_on_layer(country_code, layer_name, at_when)
        cip_records = []
        for organization in organizations:
            organization_id = organization["properties"]["organization_id"]
            organization_name = organization["properties"]["organization_name"]
            print(organization_id)

            # print(f'Results for organization {organization["properties"]["organization_name"]}:')
            report_data = get_report_data(organization_id, country_code, layer_name, at_when)
            cip_records.extend(create_cip_record(report_data, organization_id, organization_name))
            everything.extend(create_cip_record(report_data, organization_id, organization_name))

        pyexcel.save_as(records=cip_records, dest_file_name=f"{date}/{layer_name}.xlsx")

    # we use xlsx instead of xls because of the 65536 row limit in xls format.
    pyexcel.save_as(records=everything, dest_file_name=f"{date}/everything.xlsx")


def impact(metric: dict) -> str:
    severities = ["high", "medium", "low", "not_applicable", "not_testable", "error_in_test", "ok"]
    return next((severity for severity in severities if metric[severity]), "")


def explained(metric: dict) -> str:
    return "(explained)" if metric['is_explained'] else ""


if __name__ == '__main__':
    print(get_layers())
    get_metrics("NL", layer_names=[
        "province", "waterschappen", "government", "municipality", "safety_region",
        "education_university", "education_hbo", "central_government",
    ])
